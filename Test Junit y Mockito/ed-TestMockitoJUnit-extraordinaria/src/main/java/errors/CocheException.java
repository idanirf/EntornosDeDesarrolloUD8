package errors;

public class CocheException extends Exception {
    public CocheException (String message) {
        super(message);
    }
}
