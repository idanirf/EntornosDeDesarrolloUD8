package controllers;

import errors.CocheException;
import models.Coche;
import repositories.CochesRepository;
import services.CochesStorage;

import java.util.List;
import java.util.UUID;

public class CocheController  {
   private final CochesRepository cochesRepository;
   private final CochesStorage cochesStorage;


    public CocheController(CochesRepository cochesRepository, CochesStorage cochesStorage) {
        this.cochesRepository = cochesRepository;
        this.cochesStorage = cochesStorage;
    }

    public List<Coche> getCoches() {
        return cochesRepository.getAll();
    }

    public Coche getCoche(UUID id) throws CocheException {
        return cochesRepository.getById(id)
                .orElseThrow(() -> new CocheException("Coche no encontrado con id: " + id));
    }

    public Coche saveCoche(Coche coche) throws CocheException {
        checkData(coche);
        return cochesRepository.save(coche);
    }

    public Coche updateCoche(Coche coche) throws CocheException {
        checkData(coche);
        return cochesRepository.update(coche);
    }

    public void deleteCocheMatricula(Coche coche) throws CocheException {
        if (coche != null) {
            cochesRepository.delete(coche);
        }
    }

    public Coche deleteCocheById(UUID id) throws CocheException {
        return cochesRepository.delete(id)
                .orElseThrow(() -> new CocheException("Coche no encontrado con id: " + id));
    }

    private void checkData(Coche coche) throws CocheException {
        var regexMatricula = "^\\d{4}[A-Z]{3}$";
        if (coche == null) {
            throw new CocheException("Datos de coche no válidos");
        } else if (coche.getMatricula() == null || coche.getMatricula().isEmpty()) {
            throw new CocheException("Matricula de coche no puede estar en blanco");
        } else if (!coche.getMatricula().matches(regexMatricula)) {
            throw new CocheException("Matricula de coche no válida");
        }
    }

    public List<Coche> restoreData() {
        return cochesStorage.restore();
    }

    public void backupData(List<Coche> coches) {
        cochesStorage.backup(coches);
    }

}

