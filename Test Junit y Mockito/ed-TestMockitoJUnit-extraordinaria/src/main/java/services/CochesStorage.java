package services;

import models.Coche;

import java.util.List;

public interface CochesStorage extends Storage<Coche> {
    List<Coche> restore();
    boolean backup(List<Coche> list);
}

