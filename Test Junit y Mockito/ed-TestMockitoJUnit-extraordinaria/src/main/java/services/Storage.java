package services;

import java.util.List;

public interface Storage <T>{
    List<T> restore();
    boolean backup(List<T> list);
}
