package services;

import models.Coche;

import java.time.LocalDate;
import java.util.List;

public class CochesStorageImplementacion implements CochesStorage {
    @Override
    public List<Coche> restore() {
        return List.of(
                new Coche("Seat", "Ibiza", "2345LGP", LocalDate.now()),
                new Coche("BMW", "X1", "2345LGP", LocalDate.now())
        );
    }

    @Override
    public boolean backup(List<Coche> list) {
        list.forEach(System.out::println);
        return true;
    }
}
