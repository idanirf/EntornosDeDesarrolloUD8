import controllers.CocheController;
import errors.CocheException;
import models.Coche;
import repositories.CochesRepositoryImplementacion;
import services.CochesStorageImplementacion;

import java.time.LocalDate;
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
    System.out.println("Hello World!");
    System.out.println("Entornos de Desarrollo");
    System.out.println();
    info();

    }

    private static void info() {
        CocheController cocheController = new CocheController(
                new CochesRepositoryImplementacion(),
                new CochesStorageImplementacion()
        );


        System.out.println("Recuperando datos");
        var list = cocheController.restoreData();


        System.out.println("Insertando datos");
        for (Coche coche1 : list) {
            try {
                cocheController.saveCoche(coche1);
            } catch (CocheException e) {
                System.err.println(e.getMessage());
            }
        }


        System.out.println("Obteniendo todos los coches");
        var coches = cocheController.getCoches();
        coches.forEach(System.out::println);


        System.out.println("Añadiendo un coche");
        Coche coche = null;
        try {
            coche = cocheController.saveCoche(new Coche("Ferrari", "F40", "2345GRG", LocalDate.now()));
            System.out.println(coche);
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Obteniendo un coche");
        try {
            coche = cocheController.getCoche(coches.get(0).getId());
            System.out.println(coche);
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Modificando un coche");
        try {
            coche.setMatricula("3456SFC");
            coche = cocheController.updateCoche(coche);
            System.out.println(coche);
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Modificando un coche con matricula incorrecta");
        try {
            coche.setMatricula("1456SFC");
            coche = cocheController.updateCoche(coche);
            System.out.println(coche);
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Eliminando un coche");
        try {
           coche = cocheController.deleteCocheById(coche.getId());
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Borrando un coche que no existe");
        try {
            coche = cocheController.deleteCocheById(UUID.randomUUID());
            System.out.println(coche);
        } catch (CocheException e) {
            System.err.println(e.getMessage());
        }


        System.out.println("Hacemos un backup");
        cocheController.backupData(cocheController.getCoches());
    }
}
