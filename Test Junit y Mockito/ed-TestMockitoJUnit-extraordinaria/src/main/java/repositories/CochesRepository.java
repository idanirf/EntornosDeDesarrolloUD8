package repositories;

import models.Coche;

import java.util.Optional;
import java.util.UUID;

public interface CochesRepository extends CRUDRepository<Coche, UUID> {
    Optional<Coche> delete(UUID id);
}

