package repositories;

import errors.CocheException;
import models.Coche;

import java.util.*;

public class CochesRepositoryImplementacion implements CochesRepository {

    HashMap<UUID, Coche> coches = new HashMap<>();

    @Override
    public List<Coche> getAll() {
        return new ArrayList<>(coches.values());
    }

    @Override
    public Optional<Coche> getById(UUID uuid) {
        return Optional.ofNullable(coches.get(uuid));
    }

    @Override
    public Coche save(Coche entity) {
        coches.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public Coche update(Coche entity) throws CocheException {
        if (coches.containsKey(entity.getId())) {
            coches.put(entity.getId(), entity);
            return entity;
        } else {
            throw new CocheException("Coche no encontrado con id: " + entity.getId());
        }
    }

    @Override
    public void delete(Coche entity) throws CocheException {
        if (coches.containsKey(entity.getId())) {
            coches.remove(entity.getId());
        } else {
            throw new CocheException("Coche no encontrado con id: " + entity.getId());
        }
    }

    @Override
    public Optional<Coche> delete(UUID id) {
        return Optional.ofNullable(coches.remove(id));
    }
}
