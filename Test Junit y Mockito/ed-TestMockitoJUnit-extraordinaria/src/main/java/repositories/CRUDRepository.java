package repositories;

import errors.CocheException;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository <T, ID> {
    List<T> getAll();

    Optional<T> getById(ID id);

    T save(T entity);

    T update(T entity) throws CocheException;

    void delete(T entity) throws CocheException;
}
