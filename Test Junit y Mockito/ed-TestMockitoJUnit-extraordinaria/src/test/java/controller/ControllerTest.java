package controller;

import controllers.CocheController;
import errors.CocheException;
import models.Coche;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repositories.CochesRepository;
import services.CochesStorage;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

class ControllerTest {
    @Mock
    CochesRepository cochesRepository;
    @Mock
    CochesStorage cochesStorage;
    @InjectMocks
    CocheController cocheController;

    Coche cocheTest = new Coche("BMW", "X1", "7895LGM", LocalDate.now());
    Coche cocheStrorage1 = new Coche("BMW", "X2", "7742MGM", LocalDate.now());
    Coche cocheStrorage2 = new Coche("BMW", "X3", "7532MLH", LocalDate.now());

    @Test
    void checkDataMatricula() throws CocheException {
        var regexMatricula = cocheTest.getMatricula().matches("^\\d{4}[A-Z]{3}$");
        var regexMatriculaEmpty = cocheTest.getMatricula() == null || cocheTest.getMatricula().isEmpty();
        assertAll(
                ()-> assertTrue(regexMatricula, cocheTest.getMatricula()),
                ()-> assertFalse(regexMatriculaEmpty, cocheTest.getMatricula())
        );
    }

    @Test
    void checkDataMaca(){
        var regexMarca = cocheTest.getMarca() != null;
        var regexMarcaEmpty = cocheTest.getMarca() == null || cocheTest.getMarca().isEmpty();
        assertAll(
                ()-> assertTrue(regexMarca, cocheTest.getMarca()),
                ()-> assertFalse(regexMarcaEmpty, cocheTest.getMarca())
        );
    }

    @Test
    void checkDataModelo(){
        var regexModelo = cocheTest.getModelo() != null;
        var regexModeloEmpty = cocheTest.getModelo() == null || cocheTest.getModelo().isEmpty();
        assertAll(
                ()-> assertTrue(regexModelo, cocheTest.getMarca()),
                ()-> assertFalse(regexModeloEmpty, cocheTest.getMarca())
        );
    }

    @Test
    void getAll() throws CocheException {
        when(cochesRepository.getAll()).thenReturn(List.of(cocheTest));
        cocheController.saveCoche(cocheTest);
        var res = cocheController.getCoches();
        assertAll(
                ()-> assertEquals(1, res.size()),
                ()-> assertEquals(cocheTest.getId(), res.get(0).getId())
        );
        verify(cochesRepository, times(1)).getAll();

    }

    @Test
    void getCocheById() throws CocheException {
        when(cochesRepository.getById(cocheTest.getId())).thenReturn(Optional.of(cocheTest));
        cocheController.saveCoche(cocheTest);
        var res = cocheController.getCoche(cocheTest.getId());
        assertAll(
                ()->assertEquals(res.getId(), cocheTest.getId()),
                ()->assertEquals(res.getMatricula(), res.getMatricula())
        );
        verify(cochesRepository, times(1)).getById(cocheTest.getId());
    }

    @Test
    void saveCoche() throws CocheException {
        when(cochesRepository.save(cocheTest)).thenReturn(cocheTest);
        var res = cocheController.saveCoche(cocheTest);
        assertAll(
                ()-> assertEquals(cocheTest.getId(), res.getId())
        );
        verify(cochesRepository, times(1)).save(cocheTest);
    }

    @Test
    void deleteCocheById() throws CocheException {
        when(cochesRepository.delete(cocheTest.getId())).thenReturn(Optional.of(cocheTest));
        var res = cocheController.deleteCocheById(cocheTest.getId());
        assertAll(
                ()-> assertEquals(cocheTest, res),
                ()->assertEquals(cocheTest.getId(), res.getId())
        );
        verify(cochesRepository, times(1)).delete(cocheTest.getId());
    }

    @Test
    void updateCocheById() throws CocheException {
        when(cochesRepository.update(any(Coche.class))).thenReturn(cocheTest);
        var res = cocheController.updateCoche(cocheTest);
        assertAll(
                ()-> assertEquals(cocheTest, res),
                ()->assertEquals(cocheTest.getId(), res.getId()),
                ()->assertEquals(cocheTest.getMatricula(), res.getMatricula())
        );
        verify(cochesRepository, times(1)).update(cocheTest);
    }

    @Test
    void deleteCocheNoExiste() throws CocheException {
        doThrow(new CocheException("Coche no encontrado con id: " + cocheTest.getId())).when(cochesRepository).delete(cocheTest);
        var res = assertThrows(CocheException.class, ()-> cocheController.deleteCocheMatricula(cocheTest));
        assertAll(
                ()-> assertEquals(res.getMessage(), "Coche no encontrado con id: " + cocheTest.getId())
        );
        verify(cochesRepository, times(1)).delete(cocheTest);
    }

    @Test
    void getByIdNoExiste(){
        when(cochesRepository.getById(cocheTest.getId())).thenReturn(Optional.empty());
        var res = assertThrows(CocheException.class, ()-> cocheController.getCoche(cocheTest.getId()));
        assertEquals("Coche no encontrado con id: " + cocheTest.getId(), res.getMessage());
        verify(cochesRepository, times(1)).getById(cocheTest.getId());
    }

    @Test
    void restoreTest(){
        var lista = List.of(cocheStrorage1, cocheStrorage2);
        when(cochesStorage.restore()).thenReturn(lista);
        var resultado = cocheController.restoreData();
        assertAll(
                ()->assertEquals(resultado.size(), 2),
                ()->assertTrue(resultado.contains(cocheStrorage1)),
                ()->assertTrue(resultado.contains(cocheStrorage2))
        );
        verify(cochesStorage, times(1)).restore();
    }

    @Test
    void backUpTest(){
        when(cochesStorage.backup(List.of(cocheTest))).thenReturn(true);
        cocheController.backupData(List.of(cocheTest));
        verify(cochesStorage, times(1)).backup(List.of(cocheTest));
    }
}
