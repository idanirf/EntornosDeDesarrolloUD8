package repositories;

import errors.CocheException;
import models.Coche;
import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class RepositoryTest {
    CochesRepositoryImplementacion cochesRepository = new CochesRepositoryImplementacion();
    Coche cocheTest = new Coche("AUDI", "A3", "2376KLM", LocalDate.now());

    @BeforeEach
    void setUp() {
        cochesRepository.delete(cocheTest.getId());
    }

    @Test
    void getAllCoche() {
        cochesRepository.save(cocheTest);
        var result = cochesRepository.getAll();
        assertAll(
                () -> assertEquals(1, result.size()),
                () -> assertEquals(cocheTest.getId(), result.get(0).getId()),
                () -> assertEquals(cocheTest.getMatricula(), result.get(0).getMatricula())
        );
    }

    @Test
    void getCocheById() {
        cochesRepository.save(cocheTest);
        var result = cochesRepository.getById(cocheTest.getId());
        assertAll(
                ()-> assertEquals(cocheTest.getId(), result.get().getId())
        );
    }

    @Test
    void saveCoche() {
        var result = cochesRepository.save(cocheTest);
        assertAll(
                () -> assertEquals(cocheTest.getId(), result.getId()),
                () -> assertEquals(cocheTest.getMatricula(), result.getMatricula())
        );
    }

    @Test
    void deleteCoche() throws CocheException {
        cochesRepository.save(cocheTest);
        var result = cochesRepository.delete(cocheTest.getId());
        assertAll(
                () -> assertEquals(cocheTest.getId(), result.get().getId()),
                () -> assertEquals(cocheTest.getMatricula(), result.get().getMatricula())
        );
    }

    @Test
    void updateCoche() throws CocheException {
        cochesRepository.save(cocheTest);
        cocheTest.setModelo("A1");
        var result = cochesRepository.update(cocheTest);
        assertAll(
                () -> assertEquals(cocheTest, result),
                () -> assertEquals(cocheTest.getModelo(), result.getModelo())
        );
    }

    @Test
    void getByIdNoExiste(){
        var res = cochesRepository.getById(cocheTest.getId());
        Assertions.assertFalse(res.isPresent());
    }

    @Test
    void updateNoExiste() throws CocheException {
        var res = assertThrows(CocheException.class, () -> cochesRepository.update(cocheTest));
        assertEquals("Coche no encontrado con id: " + cocheTest.getId(), res.getMessage());
    }

    @Test
    void deleteNoExiste() throws CocheException {
        var res = cochesRepository.delete(cocheTest.getId());
        Assertions.assertFalse(res.isPresent());
    }
}
